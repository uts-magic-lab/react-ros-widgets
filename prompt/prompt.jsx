import React from 'react';
import './prompt-style.css';

// a text entry prompt that can send entered text as a ROS Message.
// by default, it sends the entered text as a String.
// with the property `fields`, it sends a JSON string with the
// specified fields and the entered text as 'value'.
// Shows an onscreen keyboard for compatibility with various tablets.

const defaultLayout = [
    "Q W E R T Y U I O P Delete",
    "nil A S D F G H J K L Enter",
    "Z X C V B N M ' -",
    "Space"
];

export default class ROSPrompt extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ""
        };
        this.sendMessage = (event)=>{
            if (event) {
                event.preventDefault();
            }
            let value = this.state.value;
            if (this.props.onEnter) {
                this.props.onEnter(value);
                return;
            }
            let data = value;
            if (this.props.fields) {
                value = Object.assign({}, this.props.fields, {value: value});
                data = JSON.stringify(value);
            }
            this.topic.publish({
                data: data
            });
            if (this.props.onSubmit) {
                this.props.onSubmit(value);
            }
        };
        this.onChange = (event)=>{
            this.setState({value: event.target.value});
        };
        this.onDelete = (event)=>{
            let current = this.state.value;
            if (current.length > 0) {
                current = current.substring(0, current.length - 1);
                this.setState({value: current});
            }
        };
        this.onSpace = (event)=>{
            this.setState({
                value: this.state.value + " "
            });
        }
        this.onEnter = (event)=>{
            this.sendMessage();
        };
        this.handleClick = (event)=>{
            let current = this.state.value;
            if (this["on" + event.target.value])
                this["on" + event.target.value](event);
            else {
                current += event.target.value;
                this.setState({value: current});
            }
        };
        this.autofocus = (input)=>{
            if (input && this.props.autofocus) {
                input.focus();
            }
        };
    }

    componentDidMount() {
        this.topic = this.props.ros.Topic({
            name: this.props.topic || '/web/answer',
            messageType: 'std_msgs/String'
        });
    }

    render() {
        const layout = this.props.layout || defaultLayout;
        const keyboard = layout.map((row, i)=>{
                const keys = row.split(" ");
                const buttons = keys.map((letter, j)=>{
                    return <button type="button"
                        className="ros-prompt-button"
                        key={j}
                        onClick={this.handleClick}
                        value={letter}>
                            {letter}
                    </button>;
                });
                return <div className="ros-prompt-row" key={i}>{buttons}</div>;
            }
        )

        return (
            <form className="ros-prompt" onSubmit={this.sendMessage}>
                <label className="ros-prompt-input">
                    <p>{this.props.text}</p>
                    <input type="text"
                        name="answer"
                        value={this.state.value}
                        onChange={this.onChange}
                        ref={this.autofocus}/>
                </label>
                {keyboard}
            </form>
        );
    }
}

