import React from 'react';
import ScaledImage from './scaled-image';

export default class ROSVideo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            format: "",
            data: ""
        };
    }

    componentDidMount() {
        if (!process.env.ROS_WEB_VIDEO_URI) {
            this.topic = this.props.ros.Topic({
                name: this.props.topic + '/compressed',
                messageType: 'sensor_msgs/CompressedImage'
            });
            this.topic.subscribe((msg)=>{
                console.log('SUBBED')
                this.setState(msg);
            });
        }
    }

    componentWillUnmount() {
        if (this.topic) {
            this.topic.unsubscribe();
        }
    }

    render() {
        var src = this.state.data;
        if (process.env.ROS_WEB_VIDEO_URI) {
            src = process.env.ROS_WEB_VIDEO_URI + `/stream?topic=${this.props.topic}`;
        }
        else if (this.state.format.match(/jpeg/)) {
            src = "data:image/jpeg;base64,"+this.state.data;
        }
        return <ScaledImage src={src} style={this.props.style} />
    }
}
