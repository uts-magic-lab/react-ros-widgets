# react-ros-widgets

### Installation

```shell
npm install react-ros-widgets
```

### Usage

Each widget expects a property `ros`, an instance of `ROSLIB.Ros`. It will subscribe and publish using that instance.

#### RosLabel

Shows the latest message on a specified topic.

```Javascript
import RosLabel from 'react-ros-widgets/label';
const ros = ...;

<RosLabel caption={"Speaking Text"} topic="/tts/say" ros={ros} />
```

#### RosVideo

Shows all images on a specified topic.

```Javascript
import RosVideo from 'react-ros-widgets/video';
const ros = ...;

<RosVideo topic="/wide_stereo/left/image_color" ros={ros} />
```

Supports ROS compressed image transport, and `web_video_server` transport.

#### RosPrompt

Shows a keyboard and text entry field, and publishes entered text on the specified topic.

```Javascript
import RosPrompt from 'react-ros-widgets/prompt';
const ros = ...;

<RosPrompt topic="/web/human_name" text="Enter your name:" ros={ros} />
```

Supports alternate layouts and message formats.

### TODO

- Button Widget (select) 

- Log Widget

- Graph Widget

- Publish on npm

- Use prop-types to assist with usage

- Support uncompressed video?

- Document use of web_video_server

- Support web_video_server as a prop instead of by environment variable

- Support providing topic properties as either name/messageType or pre-instantiated objects
