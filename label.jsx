import React from 'react';

export default class ROSLabel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: ''
        };
    }

    componentDidMount() {
        this.topic = this.props.ros.Topic({
            name: this.props.topic,
            messageType: 'std_msgs/String'
        });
        this.topic.subscribe((msg)=>{
            this.setState({data: msg.data});
        });
    }

    componentWillUnmount() {
        if (this.topic) {
            this.topic.unsubscribe();
        }
    }

    render() {
        return (
            <span className="ros-label">
                <span className="ros-label-caption">{this.props.caption}</span>
                {this.state.data}
            </span>
        );
    }
}
