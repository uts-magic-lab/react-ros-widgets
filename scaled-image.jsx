import React from 'react';
import './style.css';

export default class ScaledImage extends React.Component {
    render() {
        const url = encodeURI(this.props.src);
        const style = Object.assign(this.props.style || {}, {backgroundImage: 'url('+url+')'});
        return (
            <div
                className="ros-scaled-image"
                style={style}
            />
        );
    }
}
